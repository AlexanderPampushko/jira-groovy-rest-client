package pampushko.jira.rest.groovy

/**
 * You might need to import JIRA ssl certificate into your jdk/jre cacerts file:
 *  1) save JIRA certificate. E.g. in Chrome right click on https icon, click "Certificate information" link.
 *     In "Details" tab, click "Copy to File..." button.
 *  2) in jdk "bin" folder run: "keytool -importcert -keystore ./cacerts -file /file/from/the/step/above/cacert.crt -trustcacerts -alias jira_ca
 */
class JiraRestClientExample {
	static void main(String[] args) {
		// see https://docs.atlassian.com/jira/REST/server/
		// версия нашей JIRA -6.4.14, её документация - https://docs.atlassian.com/software/jira/docs/api/REST/6.4.13/
		def yourProjectIssues = "https://jira.np.ua/rest/api/2/search?jql=project=SDP&maxResults=1000"
		def createIssue = "https://jira.np.ua/rest/api/2/issue"

		println(get(yourProjectIssues))
		println(post(createIssue, createIssueContent("Have some cake", 777777)))
	}

	private
	static createIssueContent(String summary, long customField_NaumenId = 0, String description = "I love JIRA", List<String> labels = []) {
		"""
            {
                "fields": {
                    "project": { "id": "${projectId}" },
                    "summary": "${summary}",
                    "issuetype": { "id": "${taskIssueType}" },
                    "description": "${description}",
                    "customfield_14307" : ${customField_NaumenId}    
                }
            }
        """
	}

	private static get(String url) {
		def connection = url.toURL().openConnection()
		connection.addRequestProperty("Authorization", "Basic ${authString}")

		connection.setRequestMethod("GET")
		connection.doOutput = false
		connection.connect()
		connection.content.text
	}

	private static delete(String url) {
		def connection = url.toURL().openConnection()
		connection.addRequestProperty("Authorization", "Basic ${authString}")

		connection.setRequestMethod("DELETE")
		connection.doOutput = true
		connection.connect()
		connection.content.text
	}

	private static post(String url, String postContent) {
		def connection = url.toURL().openConnection()
		connection.addRequestProperty("Authorization", "Basic ${authString}")
		connection.addRequestProperty("Content-Type", "application/json")

		connection.setRequestMethod("POST")
		connection.doOutput = true
		connection.outputStream.withWriter {
			it.write(postContent)
			it.flush()
		}
		connection.connect()

		try {
			connection.content.text
		} catch (IOException e) {
			try {
				((HttpURLConnection) connection).errorStream.text
			} catch (Exception ignored) {
				throw e
			}
		}
	}

	static {
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL))
	}

	//Идентификатор типа создаваемого нами запроса
	//Тип запроса (issueTypeId) "Зміна функціоналу" имеет id 10400
	//Тип запроса (issueTypeId) "Оптимізація процесу" имеет id 10401
	private static final taskIssueType = 10400

	//Идентификатор проекта
	//Проект SDP
	private static final projectId = 14700

	//замените username и password на свои учетные данные разделенные двоеточием
	private static final authString = "username:password".bytes.encodeBase64().toString()
}
